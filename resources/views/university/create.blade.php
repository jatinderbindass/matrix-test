  @extends('adminlte::page')
  @section('title', 'Create University')
  @section('content_header')
  <!-- <h1>Create University</h1> -->
  @stop
  @section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create University</h3>
    </div>
    <form role="form" action="{{ url('dashboard/university')}}" method="post" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="_method" value="post">
        <div class="card-body">
          @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
          @endif
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Name</label>
                <input type="text" class="form-control" name="name" value="{{old('name')}}" placeholder="Enter University Name">
                @if ($errors->has('name'))
                  <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
              </div>
            </div>
          </div>
           <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Email</label>
                <input type="text" class="form-control" name="email" value="{{old('email')}}" placeholder="Enter University Email">
                @if ($errors->has('email'))
                  <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
              </div>
            </div>
          </div> 
            <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Website</label>
                <input type="text" class="form-control" name="website" value="{{old('website')}}" placeholder="Enter University Website">
                @if ($errors->has('website'))
                  <span class="text-danger">{{ $errors->first('website') }}</span>
                @endif
              </div>
            </div>
          </div>    
          <div class="row">
             <div class="col-sm-12">
               <div class="form-group">
                <label for="exampleInputFile">University Logo</label>
                <div class="input-group">
                  <div>
                    <input type="file" value="{{ old('logo_file') }}" name="logo_file" id="ct-img-file">
                    <img src="" id="profile-img-tag" width="200px" />
                     @if ($errors->has('logo_file'))
                      <span class="text-danger">{{ $errors->first('logo_file') }}</span>
                    @endif
                  </div>
                </div>
              </div>
          </div>
          </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
  </div>
    @stop