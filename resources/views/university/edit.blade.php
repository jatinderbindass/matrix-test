@extends('adminlte::page')
@section('title', 'Edit University')
@section('content_header')
<!-- <h1>Edit University</h1> -->
@stop
@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edit University</h3>
    </div>
        <div class="card-body">
        @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Unversity Name : {{ $university->name }}</label>
              </div>
            </div>
          </div>
           <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Unversity Email : {{ $university->email }}</label>
              </div>
            </div>
          </div> 
            <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Unversity Website : {{ $university->website }}</label>
              </div>
            </div>
          </div>    
          <div class="row">
             <div class="col-sm-12">
               <div class="form-group">
               @if($university->logo)
               <label for="title">Unversity Logo : <img src="{{$university->logo}}" style="height: 50px;width: 50;"></label>
               @else
               <label for="title">Unversity Logo : No Logo Provided</label>
               @endif
              </div>
          </div>
          </div>
        </div>
    </form>
  </div>
    @stop