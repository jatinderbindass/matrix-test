@extends('adminlte::page')
@section('title', 'Update University')
@section('content_header')
<!-- <h1>Update University</h1> -->
@stop
@section('content')

<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Update University</h3>
    </div>
    <form role="form" action="{{ url('dashboard/university').'/'.$university->id}}" method="post" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="id" value="{{$university->id}}">
        <div class="card-body">
        @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Name</label>
                <input type="text" class="form-control" name="name" value="{{old('name')??$university->name}}" placeholder="Enter University Name">
                @if ($errors->has('name'))
                  <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
              </div>
            </div>
          </div>
           <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Email</label>
                <input type="email" class="form-control" name="email" value="{{old('email')??$university->email}}" placeholder="Enter University Email">
                @if ($errors->has('email'))
                  <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
              </div>
            </div>
          </div> 
            <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Website</label>
                <input type="url" class="form-control" name="website" value="{{old('website')??$university->website}}" placeholder="Enter University Website">
                @if ($errors->has('website'))
                  <span class="text-danger">{{ $errors->first('website') }}</span>
                @endif
              </div>
            </div>
          </div>    
          <div class="row">
             <div class="col-sm-12">
               <div class="form-group">
                <label for="exampleInputFile">University Logo</label>
                <div class="input-group">
                  <div>
                    <input type="file" value="{{ old('logo') }}" name="logo" id="ct-img-file" value="{{$university->logo}}">
                    <img src="{{url('storage/logo/'.$university->logo)}}" style="height: 60px;width: 60px;">
                     @if ($errors->has('logo'))
                      <span class="text-danger">{{ $errors->first('logo') }}</span>
                    @endif
                  </div>
                </div>
              </div>
          </div>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>
  </div>
    @stop