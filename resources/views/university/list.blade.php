@extends('adminlte::page')
@section('title', 'University')
@section('content_header')
<!-- <h1>University Listing</h1> -->
@stop
@section('content')
<div class="row">
	<div class="col-12">
	    <div class="card">
	        <div class="card-header">
	          <h3 class="card-title">University Listing</h3>
	          <a href="{{ url('dashboard/university/create')}}" class="btn btn-sm btn-info float-right">Add University</a>
	        </div>
	        <div class="card-body">
	          	<table id="data_pagination" class="table table-bordered">
	            <thead>
	            <tr >
	            	<th>Sr No.</th>
	            	<th>Name</th>
	            	<th>Email</th>
	            	<th>Logo</th>
	            	<th>Website</th>
	            	<th>Action</th>
	            </tr>
	            </thead>
	            <tbody>
	            	@foreach($universities as $index => $university)
		            <tr>
		              <td>{{$index+1}}</td>
		              <td>{{$university->name}}</td>
		              <td>{{$university->email}}</td>
		              @if($university->logo)
		              <td><img src="{{url('storage/logo/'.$university->logo)}}" style="height: 60px;width: 60px;"></td>
		              @else
		              <td>--</td>
		              @endif
		              <td><a href="{{$university->website}}" target="_blank">{{$university->website}}</td>
		              <td>
		              	<a href="{{ url('dashboard/university/'.$university->id)}}" class="btn btn-sm btn-info">
		              	  <i class="fas fa-eye"></i>	
		              	</a>
		              	<a href="{{ url('dashboard/university') .'/'.$university->id.'/edit'}}" class="btn btn-sm btn-warning" style="margin: 2px;">
		              	  <i class="fas fa-edit"></i>	
		              	</a>
		              	<a  href="{{ url('dashboard/university') .'/'.$university->id}}" class="btn btn-sm btn-danger delete" style="margin: 2px;">
                          <i class="fas fa-trash"></i>
                      	</a>
                      </td>
                      @endforeach
		            </tr>
	        	</tbody>
	          </table>
	          {{ $universities->links() }}
			</div>
		</div>
	</div>
</div>
@stop