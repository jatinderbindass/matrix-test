  @extends('adminlte::page')
  @section('title', 'Create Student')
  @section('content_header')
  @stop
  @section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create Student</h3>
    </div>
    <form role="form" action="{{ url('dashboard/student')}}" method="post" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="_method" value="post">
        <div class="card-body">
          @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
          @endif
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">First Name</label>
                <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}" placeholder="Enter Student First Name">
                @if ($errors->has('first_name'))
                  <span class="text-danger">{{ $errors->first('first_name') }}</span>
                @endif
              </div>
            </div>
          </div>
           <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Last Name</label>
                <input type="text" class="form-control" name="last_name" value="{{old('last_name')}}" placeholder="Enter Student Last Name">
                @if ($errors->has('last_name'))
                  <span class="text-danger">{{ $errors->first('last_name') }}</span>
                @endif
              </div>
            </div>
          </div>
           <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Email</label>
                <input type="text" class="form-control" name="email" value="{{old('email')}}" placeholder="Enter Student Email">
                @if ($errors->has('email'))
                  <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
              </div>
            </div>
          </div> 
            <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Phone</label>
                <input type="text" class="form-control" name="phone" value="{{old('phone')}}" placeholder="Enter Student Phone">
                @if ($errors->has('phone'))
                  <span class="text-danger">{{ $errors->first('phone') }}</span>
                @endif
              </div>
            </div>
          </div>  
          <div class="row">
              <div class="col-sm-12">
               <div class="form-group" >
                  <label for="university_id">University</label>
                   <select class="form-control select2" name="university_id" id ="university_id">
                      <option value="" selected="">Select</option>
                      @forelse($university as $key => $value)
                      <option value="{{$value->id}}">{{ $value->name }}</option>
                      @empty
                      <option value="" selected="">No university added</option>
                      @endforelse
                   </select>
                    @if ($errors->has('university_id'))
                      <span class="text-danger">{{ $errors->first('university_id') }}</span>
                    @endif
              </div>
            </div>
          </div>  
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
  </div>
    @stop