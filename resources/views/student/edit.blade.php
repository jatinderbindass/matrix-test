@extends('adminlte::page')
@section('title', 'View Student')
@section('content_header')
<!-- <h1>View Student</h1> -->
@stop
@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">View Student</h3>
    </div>
        <div class="card-body">
        @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Student First Name : {{ $student->first_name }}</label>
              </div>
            </div>
          </div>
           <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Student Last Name : {{ $student->last_name }}</label>
              </div>
            </div>
          </div>  
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Student Email : {{ $student->email }}</label>
              </div>
            </div>
          </div> 
            <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Student Phone : {{ $student->Phone }}</label>
              </div>
            </div>
          </div>    
           <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Student University : <a href="{{ url('dashboard/university').'/'.$student->university()->first()->id}}" target="_blank">{{$student->university()->first()->name}}</a></label>
              </div>
            </div>
          </div>  
        </div>
    </form>
  </div>
    @stop