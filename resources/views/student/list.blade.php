@extends('adminlte::page')
@section('title', 'Student')
@section('content_header')
<!-- <h1>Student Listing</h1> -->
@stop
@section('content')
<div class="row">
	<div class="col-12">
	    <div class="card">
	        <div class="card-header">
	          <h3 class="card-title">Student Listing</h3>
	          <a href="{{ url('dashboard/student/create')}}" class="btn btn-sm btn-info float-right">Add Student</a>
	        </div>
	        <div class="card-body">
	          	<table id="data_pagination" class="table table-bordered">
	            <thead>
	            <tr >
	            	<th>Sr No.</th>
	            	<th>First Name</th>
	            	<th>Last Name</th>
	            	<th>Email</th>
	            	<th>Phone</th>
	            	<th>University</th>
	            	<th>Action</th>
	            </tr>
	            </thead>
	            <tbody>
	            	@foreach($students as $index => $student)
		            <tr>
		              <td>{{$index+1}}</td>
		              <td>{{$student->first_name}}</td>
		              <td>{{$student->last_name}}</td>
		              <td>{{$student->email}}</td>
		              <td>{{$student->phone}}</td>
		              <td><a href="{{ url('dashboard/university').'/'.$student->university()->first()->id}}" target="_blank">{{$student->university()->first()->name}}</td>
		              <td>
		              	<a href="{{ url('dashboard/student') .'/'.$student->id}}" class="btn btn-sm btn-info">
		              	  <i class="fas fa-eye"></i>	
		              	</a>
		              	<a href="{{ url('dashboard/student') .'/'.$student->id.'/edit'}}" class="btn btn-sm btn-warning" style="margin: 2px;">
		              	  <i class="fas fa-edit"></i>	
		              	</a>
		              	<a  href="{{ url('dashboard/student') .'/'.$student->id}}" class="btn btn-sm btn-danger delete" style="margin: 2px;">
                          <i class="fas fa-trash"></i>
                      	</a>
                      </td>
                      @endforeach
		            </tr>
	        	</tbody>
	          </table>
			</div>
		</div>
	</div>
</div>
@stop