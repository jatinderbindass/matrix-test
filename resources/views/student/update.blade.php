@extends('adminlte::page')
@section('title', 'Update Student')
@section('content_header')
<!-- <h1>Update Student</h1> -->
@stop
@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Update Student</h3>
    </div>
    <form role="form" action="{{ url('dashboard/student').'/'.$student->id}}" method="post" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="id" value="{{$student->id}}">
        <div class="card-body">
        @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
        @endif
         <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">First Name</label>
                <input type="text" class="form-control" name="first_name" value="{{old('first_name')??$student->first_name}}" placeholder="Enter Student First Name">
                @if ($errors->has('first_name'))
                  <span class="text-danger">{{ $errors->first('first_name') }}</span>
                @endif
              </div>
            </div>
          </div>
           <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Last Name</label>
                <input type="text" class="form-control" name="last_name" value="{{old('last_name')??$student->last_name}}" placeholder="Enter Student Last Name">
                @if ($errors->has('last_name'))
                  <span class="text-danger">{{ $errors->first('last_name') }}</span>
                @endif
              </div>
            </div>
          </div>
           <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Email</label>
                <input type="text" class="form-control" name="email" value="{{old('email')??$student->email}}" placeholder="Enter Student Email">
                @if ($errors->has('email'))
                  <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
              </div>
            </div>
          </div> 
            <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="title">Phone</label>
                <input type="text" class="form-control" name="phone" value="{{old('phone')??$student->phone}}" placeholder="Enter Student Phone">
                @if ($errors->has('phone'))
                  <span class="text-danger">{{ $errors->first('phone') }}</span>
                @endif
              </div>
            </div>
          </div>  
          <div class="row">
              <div class="col-sm-12">
               <div class="form-group" >
                  <label for="university_id">University</label>
                   <select class="form-control select2" name="university_id" id ="university_id">
                    @if($university->count() > 0)
                      @foreach($university as $key => $value)
                      <option value="{{$value->id}}"  <?php echo $value->id == $student->university()->first()->id ? 'selected' :'' ?>>{{ $value->name }}</option>
                      @endforeach
                    @else
                      <option value="0" selected="">No University Added</option>
                    @endif
                   </select>
                    @if ($errors->has('university_id'))
                      <span class="text-danger">{{ $errors->first('university_id') }}</span>
                    @endif
              </div>
            </div>
          </div>  
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>
  </div>
    @stop