<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Student;
use App\University;

class Student extends Model
{  
    protected $fillable = [
        'first_name',
        'last_name',
        'university_id',
        'email',
        'phone',
    ];


    public function university()
    {
        return $this->belongsTo(University::class,'university_id', 'id');
    }  
}
