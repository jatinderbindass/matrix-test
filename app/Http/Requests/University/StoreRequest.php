<?php

namespace App\Http\Requests\University;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|min:2|max:255',
            'email'     => 'required|unique:users|email',
            'website'   => 'required',
            'university_logo'      => 'mimes:jpeg,bmp,png,jpg|max:5000|dimensions:max_width=100,max_height=100'
        ];
    }
}
