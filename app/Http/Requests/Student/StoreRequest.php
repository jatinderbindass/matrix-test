<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'first_name'      => 'required|min:2|max:255',
            'last_name'       => 'required|min:2|max:255',
            'email'           => 'required|unique:users|email',
            'phone'           => 'required|numeric|min:9',
            'university_id'   => 'required|exists:users,id'
        ];
    }
}
