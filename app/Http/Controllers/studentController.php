<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\University;
class studentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::latest()->paginate(5);
        return view('student.list',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $university = University::all();
        return view('student.create',compact('university'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',            
            'university_id' => 'required',            
        ], [
            'first_name.required' => 'First name field is required.',
            'last_name.required' => 'Last name field is required.',
            'university_id.required' => 'Please create university first.',
        ]);
        $student = new Student([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'university_id' => $request->get('university_id'),
        ]);
        $student->save();      
        return redirect('dashboard/student')->with('status', 'Student added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::where('id',$id)->first();
        return view('student.edit',compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::where('id',$id)->first();
        $university = University::all();
        return view('student.update',compact('student','university'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',            
            'university_id' => 'required',            
        ], [
            'first_name.required' => 'First name field is required.',
            'last_name.required' => 'Last name field is required.',
            'university_id.required' => 'Please create university first.',
        ]);
        $student = Student::where('id',$id)->update([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'university_id' => $request->get('university_id'), 
        ]);           
        return redirect('dashboard/student')->with('status', 'Student updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::where('id',$id)->delete();
        return response()->json(['status' => 200]);
    }
}
