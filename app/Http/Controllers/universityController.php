<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\University;

class universityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $universities = University::latest()->paginate(5);

        return view('university.list',compact('universities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('university.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'university_logo' => 'mimes:jpeg,bmp,png,jpg|max:5000|dimensions:max_width=100,max_height=100'
        ], [
            'name.required' => 'Name field is required.',
        ]);
        if($request->hasFile('logo_file')){
            $file = $request->file('logo_file');
            $fileName = time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('logo', $fileName);
            $request->merge(['logo' => $fileName]);
        }
        
        $university = University::create($request->all());
        $university->save();
        $details = [
            'title' => 'Mail from jatinder makker ',
            'body' => 'This is for testing email using smtp mailtrap. University Created successfully'
        ];
   
        \Mail::to('matrixmtestcandidate@mailinator.com')->send(new \App\Mail\MailTesting($details));
        return redirect('dashboard/university')->with('status', 'University added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $university = University::where('id',$id)->first();
        return view('university.edit',compact('university'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $university = University::where('id',$id)->first();
        return view('university.update',compact('university'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {    
        $request->validate([
            'name' => 'required',
            'university_logo' => 'mimes:jpeg,bmp,png,jpg|max:5000|dimensions:max_width=100,max_height=100'
        ], [
            'name.required' => 'Name field is required.',
        ]);
        $university = University::where('id',$id)->first();
        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $fileName = 'logo-'.time().'.'.$file->getClientOriginalExtension();
            $path = $file->storeAs('logo', $fileName);
            $university->logo = $fileName;
        }
        $university->name = $request->get('name');
        $university->email = $request->get('email');
        $university->website = $request->get('website');
        $university->save();
        return redirect('dashboard/university')->with('status', 'University updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $university = University::where('id',$id)->delete();
        return response()->json(['status' => 200]);
    }
}
