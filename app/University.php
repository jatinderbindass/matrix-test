<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\User;

class University extends Model
{  
    protected $fillable = [
        'name',
        'email',
        'logo',
        'website',
    ];

}