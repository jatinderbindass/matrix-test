<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', function () {
    return view('welcome');
});
Route::get('dashboard','adminController@index');
Route::group(['prefix' => 'dashboard','middleware' => 'auth'], function() {
	Route::resource('student',studentController::class);
	Route::resource('university',universityController::class);	
});