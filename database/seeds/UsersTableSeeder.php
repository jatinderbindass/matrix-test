<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where([
        	'email' => 'testing@testing.com',
            ])->first();
    	if(!$user){
	        $user = User::create([
	        	'name' => 'Testing', 
	        	'email' => 'testing@testing.com',
	        	'password' => Hash::make('admin'),
	        	'remember_token' => Str::random(10),
	        ]);

    	}
    }
}
